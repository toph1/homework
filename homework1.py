# -*- coding: utf-8 -*-
"""
Spyder Editor

Hi! Welcome to my Homework. I'm Christopher Cook.

Note that I removed all directory references so just make sure the default 
directory is where the data is stored. I apologize for the general lack of 
comments, but I procrastinated doing this to the last moment and it's literally
too late now. Thanks!
"""

import sqlite3 as sql
import csv
import numpy as np
from matplotlib import pyplot

def manage(tablename):
    try:
        with sql.connect(tablename) as connection:
            cursor = connection.cursor()
            cursor.execute('DROP TABLE IF EXISTS MajorInfo')
            cursor.execute('DROP TABLE IF EXISTS CourseInfo')
            cursor.execute('DROP TABLE IF EXISTS StudentInfo')
            cursor.execute('DROP TABLE IF EXISTS StudentGrades')

            cursor.execute('CREATE TABLE MajorInfo (MajorID INTEGER, MajorName TEXT)')
            cursor.execute('CREATE TABLE CourseInfo (CourseID INTEGER, CourseName TEXT)')
            cursor.execute('CREATE TABLE StudentInfo (StudentID INTEGER, StudentName TEXT, MajorID INTEGER)')
            cursor.execute('CREATE TABLE StudentGrades (StudentID INTEGER, CourseID INTEGER, Grade TEXT)')
    finally:
        connection.close()
        
def addinfo(tablename):
    try:
        with sql.connect(tablename) as connection:
            cursor = connection.cursor()
            
            with open('student_info.csv', 'r') as file:
                rows = list(csv.reader(file))
                cursor.executemany('INSERT INTO StudentInfo VALUES(?,?,?)',rows)
                cursor.execute('UPDATE StudentInfo SET MajorID = NULL where MajorID == -1')
            
            with open('student_grades.csv', 'r') as file:
                rows = list(csv.reader(file))
                cursor.executemany('INSERT INTO StudentGrades VALUES(?,?,?)',rows)
                
            
            #Majorinfo
            rows = [('1','Math'),('2','Science'),('3','Writing'),('4','Art')]
            cursor.executemany('INSERT INTO MajorInfo VALUES(?,?)',rows)
            
            #CourseInfo
            rows = [('1','Calculus'),('2','English'),('3','Pottery'),('4','History')]
            cursor.executemany('INSERT INTO CourseInfo VALUES(?,?)',rows)
            
    finally:
        connection.close()
        
def quake(tablename):
    try:
        with sql.connect(tablename) as connection:
            cursor = connection.cursor()
            cursor.execute('DROP TABLE IF EXISTS USEarthquakes')

            cursor.execute('CREATE TABLE USEarthquakes (Year INTEGER, Month INTEGER, Day INTEGER, Hour INTEGER, Minute INTEGER, Second INTEGER, Latitude REAL, Longitude REAL, Magnitude REAL)')
    
            with open('us_earthquakes.csv', 'r') as file:
                rows = list(csv.reader(file))
                cursor.executemany('INSERT INTO USEarthquakes VALUES(?,?,?,?,?,?,?,?,?)',rows)
                cursor.execute('DELETE FROM USEarthquakes WHERE Magnitude == 0')
                #times = [('Day','Day'),('Hour','Hour'),('Minute','Minute'),('Second','Second')]
                cursor.execute('UPDATE USEarthquakes SET Day = NULL WHERE Day == 0')
                cursor.execute('UPDATE USEarthquakes SET Hour = NULL WHERE Hour == 0')
                cursor.execute('UPDATE USEarthquakes SET Minute = NULL WHERE Minute == 0')
                cursor.execute('UPDATE USEarthquakes SET Second = NULL WHERE Second == 0')

    finally:
        connection.close()
        
def askStudentA(tablename):
    try:
        with sql.connect(tablename) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT SI.StudentName, CI.CourseName "
                           "FROM StudentInfo AS SI, CourseInfo AS CI, StudentGrades as SG "
                           "WHERE SI.StudentID == SG.StudentID AND SG.CourseID == CI.CourseID AND (SG.Grade == 'A' OR SG.Grade == 'A+')")
            data = cursor.fetchall()

    finally:
        connection.close()
    
    return data

def quakes_comp(tablename):
    try:
        with sql.connect(tablename) as connection:
            cursor = connection.cursor()
            cursor.execute('SELECT Magnitude FROM USEarthquakes WHERE YEAR >= 1800 AND YEAR <= 1899')
            cen19th = cursor.fetchall()
            cursor.execute('SELECT Magnitude FROM USEarthquakes WHERE YEAR >= 1900 AND YEAR <= 1999')
            cen20th = cursor.fetchall()
            cursor.execute('SELECT AVG(Magnitude) FROM USEarthquakes')
            avg = cursor.fetchall()
            
            #do histogram
            
            npcen19th = np.ravel(cen19th)
            npcen20th = np.ravel(cen20th)
            
            pyplot.hist(npcen20th)
            pyplot.hist(npcen19th)
            pyplot.show()

    finally:
        connection.close()
    
    return avg[0][0]

if __name__ == '__main__':

    manage('my_database.db')
    addinfo('my_database.db')
    quake('quakes.db')
    print(askStudentA('my_database.db'))
    print(quakes_comp('quakes.db'))

'''
try:

    with sql.connect('my_database.db') as conn:
        cur = conn.cursor()
        for row in cur.execute('SELECT * FROM MajorInfo;'):
            print(row)
        cur.execute('SELECT * FROM StudentInfo;')
        print([d[0] for d in cur.description])
except:
    conn.close()
'''
    